//
// Created by Alex Gold on 2019-03-27.
// Copyright (c) 2019 Alex Gold. All rights reserved.
//

import UIKit
import Firebase
//import FirebaseData

final class FirebaseModule {
	static let shared = FirebaseModule()
	let name = "listfromjson"
	let reference : DatabaseReference?
	var moviesArray : [MovieModel] = []


	private init() {
		reference = Database.database().reference()
	}

	func readMovies(completion: @escaping(([MovieModel]) -> ())) {
		guard let ref = reference else { return }
		
		if moviesArray.isEmpty {
			ref.observe(.value) { snapshot in
				
				let enumerator = snapshot.children
				let children = (enumerator.allObjects as? [DataSnapshot])!
				
				for i in 0..<children.count {
					let child = children[i]
					let movie = self.convertSnapShotTo(type: MovieModel.self, snapshot: child)
					self.moviesArray.append(movie ?? MovieModel())
				}
				//do
				completion(self.moviesArray)
			}
		} else {
			completion(self.moviesArray)
		}
		
		
	}
	
	func convertSnapShotTo<T: Codable>(type: T.Type, snapshot: DataSnapshot) -> T? {
		do {
			let jsonData = try JSONSerialization.data(withJSONObject: snapshot.value! as! [String: Any], options: .prettyPrinted)
			let returnValue = try JSONDecoder().decode(type, from: jsonData)
			return returnValue
		} catch {
			print("Error converting snapshot to \(type), \(error)")
		}
		return nil
	}
}
