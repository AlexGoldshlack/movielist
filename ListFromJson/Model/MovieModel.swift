//
//  MovieModel.swift
//  ListFromJson
//
//  Created by Alex Gold on 27/03/2019.
//  Copyright © 2019 Alex Gold. All rights reserved.
//

import Foundation

class MovieModel: Codable {

	var image: String
	var rating: Double
	var releaseYear: Int
	var title: String
	var genre: [String]

	init(image: String, rating: Double, releaseYear: Int, title: String, genre: [String]) {
		self.image = image
		self.rating = rating
		self.releaseYear = releaseYear
		self.title = title
		self.genre = genre
	}
	
	init() {
		self.image = ""
		self.rating = 0
		self.releaseYear = 0
		self.title = ""
		self.genre = [""]
	}

	required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
		self.rating = try container.decodeIfPresent(Double.self, forKey: .rating) ?? 0
		self.releaseYear = try container.decodeIfPresent(Int.self, forKey: .releaseYear) ?? 0
		self.title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
		self.genre = try container.decodeIfPresent([String].self, forKey: .genre) ?? [""]
	}
}
