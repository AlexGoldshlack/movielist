//
//  MovieDetailsStackView.swift
//  ListFromJson
//
//  Created by Alex Gold on 28/03/2019.
//  Copyright © 2019 Alex Gold. All rights reserved.
//

import UIKit

class MovieDetailsStackView: UIStackView {
	
	let releaseLabel: UILabel = {
		let label = UILabel(frame: .zero)
		return label
	}()
	
	let ratingLabel: UILabel = {
		let label = UILabel(frame: .zero)
		return label
	}()
	
	let genreLabel: UILabel = {
		let label = UILabel(frame: .zero)
		label.numberOfLines = 0
		return label
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		distribution = .fillEqually
		axis = .vertical
		
		[releaseLabel, ratingLabel, genreLabel, UIView()].forEach { view in
			addArrangedSubview(view)
		}
		
		isLayoutMarginsRelativeArrangement = true
	}
	
	required init(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
