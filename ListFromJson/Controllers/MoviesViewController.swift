//
//  MoviesViewController.swift
//  ListFromJson
//
//  Created by Alex Gold on 27/03/2019.
//  Copyright © 2019 Alex Gold. All rights reserved.
//

import UIKit

class MoviesViewController: UITableViewController {
	
	var movies: [MovieModel]?
	
	override func viewDidLoad() {
		setup()
		FirebaseModule.shared.readMovies { array in
			self.movies = array
			self.tableView.reloadData()
		}
	}
	
	fileprivate func setup() {
		view.backgroundColor = .purple
		navigationItem.title = "Movie list"
		tableView.separatorStyle = .none
		tableView.tableFooterView = UIView()
		tableView.register(MovieCell.self, forCellReuseIdentifier: "cell")
	}
	
	//MARK: tableView callbacks
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MovieCell
		let movie = movies?[indexPath.row]
		cell.textLabel?.text = movie?.title ?? ""
		return cell
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return movies?.count ?? 0
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let controller = MovieDetailsController()
		controller.movie = movies![indexPath.row]
		navigationController?.pushViewController(controller, animated: true)
	}
}
