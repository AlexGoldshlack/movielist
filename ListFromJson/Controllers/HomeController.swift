//
//  HomeController.swift
//  ListFromJson
//
//  Created by Alex Gold on 27/03/2019.
//  Copyright © 2019 Alex Gold. All rights reserved.
//

import UIKit

class HomeController: UIViewController {
	
	let button: UIButton = {
		let bt = UIButton(type: .system)
		bt.setTitle("Go to Movies", for: .normal)
		bt.titleLabel?.font = UIFont.systemFont(ofSize: 32, weight: .heavy)
		bt.backgroundColor = .white
		bt.setTitleColor(.black, for: .normal)
		bt.addTarget(self, action: #selector(handleMovieButton), for: .touchUpInside)
		return bt
	}()
	
	override func viewDidLoad() {
		setupUI()
		FirebaseModule.shared.readMovies {_ in }
	}
	
	
	fileprivate func setupUI() {
		view.backgroundColor = .white
		navigationItem.title = "Home"
		view.addSubview(button)
		button.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 50, left: 50, bottom: 0, right: 50))
	}
	
	@objc fileprivate func handleMovieButton() {
		print("To movies")
		navigationController?.pushViewController(MoviesViewController(), animated: true)
	}
}
