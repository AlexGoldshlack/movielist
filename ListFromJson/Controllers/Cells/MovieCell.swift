//
//  MovieCell.swift
//  ListFromJson
//
//  Created by Alex Gold on 27/03/2019.
//  Copyright © 2019 Alex Gold. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		backgroundColor = .clear
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
