//
//  MovieDetailsController.swift
//  ListFromJson
//
//  Created by Alex Gold on 27/03/2019.
//  Copyright © 2019 Alex Gold. All rights reserved.
//

import UIKit

class MovieDetailsController: UIViewController {
	var movie: MovieModel?
	let detailsStackView = MovieDetailsStackView()
	
	let imageView: UIImageView = {
		let imageView = UIImageView()
		imageView.backgroundColor = .black
		imageView.layer.cornerRadius = 8
		imageView.clipsToBounds = true
		imageView.contentMode = .scaleAspectFit
		return imageView
	}()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .lightGray
		navigationItem.title = movie!.title
		setupUI()
    }
	
	fileprivate func addStackView() {
		view.addSubview(detailsStackView)
		detailsStackView.anchor(top: imageView.bottomAnchor, leading: imageView.leadingAnchor, bottom: view.bottomAnchor, trailing: imageView.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 50, right: 0))
		
		
		var genreString = "Genres: "
		movie?.genre.forEach({ (string) in
			genreString.append("\(string), ")
		})
		detailsStackView.genreLabel.text = genreString
		
		detailsStackView.releaseLabel.text = "Release Year: \(movie!.releaseYear)"
		detailsStackView.ratingLabel.text = "Rating: \(movie!.rating)"
	}
	
	fileprivate func loadImage() {
		guard let imageUrl = movie?.image else { return }
		
		DispatchQueue.main.async {
			let url = URL(string: imageUrl)!
			let data = try? Data(contentsOf: url)
			
			if let imageData = data {
				let image = UIImage(data: imageData)
				self.imageView.image = image
			}
		}
	}
	
	fileprivate func setupUI() {
		view.addSubview(imageView)
		imageView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 15, left: 50, bottom: 0, right: 50), size: .init(width: 0, height: 240))
		
		addStackView()
		loadImage()
	}
}
